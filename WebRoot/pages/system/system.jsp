<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<style>
		.input-text {width: 400px;}
	</style>
</head>

<body>
	<div class="wrapper">
	
		<div class="tabbable tabs-left">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#tab1" data-toggle="tab">修改密码</a></li>
				<li onclick="leftMenu($(this));" url="<c:url value="/pages/system/systemName.jsp" />"><a href="#tab2" data-toggle="tab">系统名称</a></li>
				<li onclick="leftMenu($(this));" url="<c:url value="/pages/system/bugCode.jsp" />"><a href="#tab3" data-toggle="tab">编号规则</a></li>
				<li onclick="leftMenu($(this));" url="<c:url value="/pages/system/field.jsp" />"><a href="#tab4" data-toggle="tab">字段管理</a></li>
			</ul>
			
			<div class="tab-content">
				<div id="tab1" class="tab-pane active">
					<div id="btnDiv">
						<button class="btn" onclick="modifyPwd();">确认修改</button>
					</div>
				
					<table class="edit-table">
						<tr>
							<td class="left-td" width="80px">原始密码</td>
							<td><input id="pwd1" type="password" class="input-text input-require" /></td>
						</tr>
						<tr>
							<td class="left-td">新密码</td>
							<td><input id="pwd2" type="password" class="input-text input-require" /></td>
						</tr>
						<tr>
							<td class="left-td">确认密码</td>
							<td><input id="pwd3" type="password" class="input-text input-require" /></td>
						</tr>
					</table>
				</div>
				
				<div id="tab2" class="tab-pane"></div>
				<div id="tab3" class="tab-pane"></div>
				<div id="tab4" class="tab-pane"></div>
			</div>
		</div>
		
	</div>
	
	<div id="dialog-ok" class="hide" title="修改密码">
		<p>修改密码成功！</p>
	</div>
	
	<script>
		function modifyPwd() {
			if(!checkForm())  return;
			
			if($("#pwd2").val() != $("#pwd3").val()) {
				$("#pwd3").parent().append("<div class='outline validate'>新密码与确认密码不匹配</div>");
				return;
			}
			
			_remoteCall("user/unlock.do", {password: $("#pwd1").val()}, function(data) {
				if(data == "Y") {
					_remoteCall("user/changePwd.do", {password: $("#pwd2").val()}, function() {
						$( "#dialog-ok" ).dialog({
							resizable: false,
						    height: 200,
						    modal: true,
						    buttons: {
						        "確定": function() {
						        	$(this).dialog("close");
						        	parent.location.href = parent.basePath + "user/logout.do";
						        }
							}
						});
						
						$("#pwd1, #pwd2, #pwd3").val("");
					});	
					
				} else {
					$("#pwd1").parent().append("<div class='outline validate'>原始密码错误</div>");
				}
			});
		}
	</script>
	
</body>
</html>
