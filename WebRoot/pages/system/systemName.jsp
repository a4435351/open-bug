<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<style>
		.input-text {width: 400px;}
	</style>
</head>

<body>
	<div class="wrapper">
	
		<div class="tabbable tabs-left">
			<ul class="nav nav-tabs">
				<li onclick="leftMenu($(this));" url="<c:url value="/pages/system/system.jsp" />"><a href="#tab1" data-toggle="tab">修改密码</a></li>
				<li class="active"><a href="#tab2" data-toggle="tab">系统名称</a></li>
				<li onclick="leftMenu($(this));" url="<c:url value="/pages/system/bugCode.jsp" />"><a href="#tab3" data-toggle="tab">编号规则</a></li>
				<li onclick="leftMenu($(this));" url="<c:url value="/pages/system/field.jsp" />"><a href="#tab4" data-toggle="tab">字段管理</a></li>
			</ul>
			
			<div class="tab-content">
				<div id="tab2" class="tab-pane active">
					<div id="btnDiv">
						<button class="btn" onclick="modifySystemName();">确认修改</button>
					</div>
					
					<table class="edit-table">
						<tr>
							<td class="left-td" width="80px">系统名称</td>
							<td><input id="systemName" type="text" class="input-text input-require" /></td>
						</tr>
					</table>
				</div>
				
				<div id="tab1" class="tab-pane"></div>
				<div id="tab3" class="tab-pane"></div>
				<div id="tab4" class="tab-pane"></div>
			</div>
		</div>
	</div>
	
	<div id="dialog-ok" class="hide" title="修改密码">
		<p>修改系统名称成功，重新登录系统后生效！</p>
	</div>
	
	<script>
		_remoteCall("system/getSystemName.do", null, function(data) {
			$("#systemName").val(data);
		});
		
		function modifySystemName() {
			if(!checkForm())  return;
			
			_remoteCall("system/modifySystemName.do", {systemName: $("#systemName").val()}, function() {
				$("#dialog-ok").dialog({
					resizable: false,
				    height: 200,
				    modal: true,
				    buttons: {
				        "確定": function() {
				        	$(this).dialog("close");
				        }
					}
				});
			});
		}
	</script>
</body>
</html>
